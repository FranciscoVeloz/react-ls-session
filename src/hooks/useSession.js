const useSession = () => {
  const getSession = () => {
    const session = window.localStorage.getItem("session");
    return JSON.parse(session);
  };

  const isLoggedIn = () => {
    const session = getSession();
    if (session === null) return false;
    return true;
  };

  return {
    getSession,
    isLoggedIn,
  };
};

export default useSession;
