import React from "react";
import useSession from "../hooks/useSession";
import { Navigate } from "react-router-dom";

const IsNotLoggedIn = ({ view: View }) => {
  const session = useSession();

  if (!session.isLoggedIn()) return <View />;
  return <Navigate to="/" replace={true} />;
};

export default IsNotLoggedIn;
