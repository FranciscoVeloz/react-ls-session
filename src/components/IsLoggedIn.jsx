import React from "react";
import useSession from "../hooks/useSession";
import { Navigate } from "react-router-dom";

const IsLoggedIn = ({ view: View }) => {
  const session = useSession();
  
  if (!session.isLoggedIn()) return <Navigate to="/login" replace={true} />;
  return <View />;
};

export default IsLoggedIn;
