import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const initialState = {
  email: "",
  password: "",
};

const Login = () => {
  const navigate = useNavigate();
  const [login, setLogin] = useState(initialState);

  const handleChange = (key, value) => setLogin({ ...login, [key]: value });

  const handleSubmit = (e) => {
    e.preventDefault();

    window.localStorage.setItem("session", JSON.stringify(login));
    navigate("/");
  };

  return (
    <div className="container my-4">
      <div className="row">
        <div className="col-lg-4 col-md-6 col-12 mx-auto">
          <form className="card" onSubmit={handleSubmit}>
            <div className="card-header text-center h3">Log in</div>
            <div className="card-body">
              <div className="mb-2">
                <input
                  type="email"
                  placeholder="Email"
                  className="form-control"
                  value={login.email}
                  onChange={(e) => handleChange("email", e.target.value)}
                />
              </div>

              <div className="mb-3">
                <input
                  type="password"
                  placeholder="Password"
                  className="form-control"
                  value={login.password}
                  onChange={(e) => handleChange("password", e.target.value)}
                />
              </div>
              <div className="d-grid gap-2">
                <button type="submit" className="btn btn-primary text-center">
                  Log in
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
