import React from "react";

const Home = () => {
  return (
    <div className="container my-4">
      <div className="row">
        <h1 className="text-center">Hello world</h1>
        <h2 className="text-center">
          This is a example of auth with localstorage
        </h2>
      </div>
    </div>
  );
};

export default Home;
