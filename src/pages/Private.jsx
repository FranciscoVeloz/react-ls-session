import React from "react";

const Private = () => {
  return (
    <div className="container my-4">
      <div className="row">
        <h1 className="text-center">Private page</h1>
        <h2 className="text-center">
          This is a private page, you can only access to here with a session
        </h2>
      </div>
    </div>
  );
};

export default Private;
