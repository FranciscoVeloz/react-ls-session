import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

//Pages
import Home from "./pages/Home";
import Login from "./pages/Login";
import Private from "./pages/Private";

//Components
import Layout from "./components/Layout";
import IsLoggedIn from "./components/IsLoggedIn";
import IsNotLoggedIn from "./components/IsNotLoggedIn";

//Importing styles
import "./App.css";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<IsNotLoggedIn view={Login} />} />
          <Route path="/private" element={<IsLoggedIn view={Private} />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
